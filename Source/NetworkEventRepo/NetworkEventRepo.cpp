// Copyright Epic Games, Inc. All Rights Reserved.

#include "NetworkEventRepo.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, NetworkEventRepo, "NetworkEventRepo" );
 