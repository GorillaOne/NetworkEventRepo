// Copyright Epic Games, Inc. All Rights Reserved.

#include "NetworkEventRepoGameMode.h"
#include "NetworkEventRepoCharacter.h"
#include "UObject/ConstructorHelpers.h"

ANetworkEventRepoGameMode::ANetworkEventRepoGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPerson/Blueprints/BP_ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
